import boto3
import os
import sys
from boto import ec2
import boto.s3.connection
import datetime
from boto.utils import get_instance_metadata
import boto.rds
import argparse
from pprint import pprint
from boto import rds
import xlwt
from xlwt import Workbook
from boto.exception import EC2ResponseError
import boto.elasticache


ACCESS_KEY = None
SECRET_KEY = None
regions = None


#regions = 'ap-southeast-1|us-east-2|us-east-1|us-west-1|us-west-2|ap-south-1|ap-northeast-2|ap-southeast-2|ap-northeast-1|ca-central-1|eu-central-1|eu-west-1|eu-west-2'

#filepath='/Users/andmac37/Desktop/ketto.xls'
#filepath='/Users/andmac37/Documents/AWS-Infrasheet-Automation/ketto-Alarms.xls'

wb = Workbook()

def open_file(filepath,sheetname):
  """
  Opens the output files, promts whether to overwrite
  """
  goaheadandopen = True
  if os.path.exists(filepath):
    if os.path.isfile(filepath):
      valid = {'yes': True, 'y': True,
               'no': False, 'n': False}

      while True:
        #sys.stdout.write('file %s exists, overwrite it? [y/n] ' % filepath)
        sys.stdout.write('Add sheet %s ? [y/n] ' % sheetname)
        choice = raw_input().lower()
        if choice in valid.keys():
          if not valid[choice]:
            goaheadandopen = False
          break
        sys.stdout.write('Please respond with \'yes\' or \'no\' (or \'y\' or \'n\').\n')
    else:  # folder
      sys.stdout.write('%s exists but nt a regular file. Aborting...\n')
      goaheadandopen = False

  if not goaheadandopen:
    return None

  try:
    f = file(filepath, 'wt')
  except Exception, e:
    f = None
    sys.stderr.write('Could not open file %s. reason: %s\n' % (filepath, e))

  return f

def list_alarms ():
    # opens file
    f = open_file(filepath,"List Alarms")
    if not f:
        return False

    i = 1
    j = 0

    sheet1 = wb.add_sheet('List Alarms')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet1.write(0, 0, 'Sr.No', style)
    sheet1.write(0, 1, 'Region', style)
    sheet1.write(0, 2, 'Alarm Name', style)
    sheet1.write(0, 3, 'Alarm Description', style)
    sheet1.write(0, 4, 'Evaluation Periods', style)
    sheet1.write(0, 5, 'Comparison Operator', style)
    sheet1.write(0, 6, 'Datapoints To Alarm', style)
    sheet1.write(0, 7, 'Threshold', style)
    sheet1.write(0, 8, 'Period', style)
    sheet1.write(0, 9, 'Metric Name', style)
    sheet1.write(0, 10, 'Namespace', style)
    sheet1.write(0, 11, 'Statistic', style)
    sheet1.write(0, 12, 'State Value', style)
    sheet1.write(0, 13, 'State Reason', style)
    sheet1.write(0, 14, 'Alarm ARN', style)

    #region = 'ap-southeast-1'

    region_list = regions.split('|')

    # go over all regions in list
    for region in region_list:
        print region
        client = boto3.client('cloudwatch',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region
                              )

        # Create a reusable Paginator
        paginator = client.get_paginator('describe_alarms')

        # tables = dynamodb.list_tables()
        list_metrics_info = client.describe_alarms()

        #print(list_metrics_info['NextToken'])

        page_iterator = paginator.paginate()

        for page in page_iterator:
            #print(page['Contents'])
        # for table in tables["TableNames"]:
        # print "tables" + existing_tables["TableName"]

            for metrics_info in page['MetricAlarms']:

                sheet1.write(i, j, i)
                j = j + 1
                sheet1.write(i, j, region)
                j = j + 1
                sheet1.write(i, j, metrics_info['AlarmName'])
                j = j + 1
                if 'AlarmDescription' not in metrics_info:
                    j = j + 1
                else:
                    sheet1.write(i, j, metrics_info['AlarmDescription'])
                    j = j + 1
                sheet1.write(i, j, metrics_info['EvaluationPeriods'])
                j = j + 1
                sheet1.write(i, j, metrics_info['ComparisonOperator'])
                j = j + 1

           # if metrics_info['Namespace'] in ['AWS/EC2','CWAgent'] and metrics_info['MetricName'] != 'CPUUtilization' or metrics_info['MetricName'] != 'StatusCheckFailed' :
                #if metrics_info['Namespace'] in ['AWS/EC2','CWAgent'] and metrics_info['MetricName'] not in ['CPUUtilization','StatusCheckFailed','StatusCheckFailed_Instance','StatusCheckFailed_System']:
                if 'DatapointsToAlarm' in metrics_info:
                    sheet1.write(i, j, metrics_info['DatapointsToAlarm'])
                    j = j + 1

                else:
                    sheet1.write(i, j, "NULL")
                    j = j + 1

                sheet1.write(i, j, metrics_info['Threshold'])
                j = j + 1
                sheet1.write(i, j, metrics_info['Period'])
                j = j + 1
                sheet1.write(i, j, metrics_info['MetricName'])
                j = j + 1
                sheet1.write(i, j, metrics_info['Namespace'])
                j = j + 1
                sheet1.write(i, j, metrics_info['Statistic'])
                j = j + 1
                sheet1.write(i, j, metrics_info['StateValue'])
                j = j + 1
                sheet1.write(i, j, metrics_info['StateReason'])
                j = j + 1
                sheet1.write(i, j, metrics_info['AlarmArn'])

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()
    return True

if __name__ == '__main__':
    # ec2_connection (ACCESS_KEY,SECRET_KEY,regions)

    parser = argparse.ArgumentParser(description='Creates a Excel report about AWS Resources.')
    parser.add_argument('--access_key',required = True, help='AWS API access key.  If missing default is used')
    parser.add_argument('--secret_key',required = True, help='AWS API secret key.  If missing default is used')
    parser.add_argument('--file', required=True, help='Path for output Excel file')
    parser.add_argument('--regions', required=True, help='AWS regions to create the report on, can add multiple with | as separator. Default will assume all regions')
    args = parser.parse_args()

    #args.access_key,args.secret_key,args.file

    ACCESS_KEY = args.access_key
    SECRET_KEY = args.secret_key
    filepath = args.file
    regions = args.regions
    if(regions.lower() == 'all'):
        regions = 'ap-southeast-1|us-east-2|us-east-1|us-west-1|us-west-2|ap-south-1|ap-northeast-2|ap-southeast-2|ap-northeast-1|ca-central-1|eu-central-1|eu-west-1|eu-west-2'
    else:
        regions = args.regions

    print(regions)

    list_alarms()
