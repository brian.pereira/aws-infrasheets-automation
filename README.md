# Project Title

Automation script to generate Infrastructure Sheets & Monitoring Sheet for AWS Clients

## List of Services covered in the script

* EC2
* EBS
* Lambda
* S3
* SNS
* SQS
* Api Gateway
* Load Balancer(Classic + App)
* Route 53
* Security Group Details
* ACM
* CloudTrail
* CloudFront
* NAT Gateway
* Redis
* IAM
* RDS
* Dynamo DB
* Redshift


## Getting Started

This script is used to download the existing infrastructure details of an AWS Account

They will be helpful to someone who regularly reviews the aws infra without having to log into the console and check the infra each and every time and note down the aws service details in excel sheets.They will be further helpful in optimizing human resource by not wasting time in making manual entries and doing donkey labou


### Prerequisites

What things you need to install the software and how to install them

### Install Python 2.7

### Refreshing the repositories
sudo apt update
### Update software
sudo apt upgrade

### Install Python and necessary packages.

### Install pip for 2.7 and then python 2.7 itself
sudo apt install python-pip
sudo apt install python2.7

### Install xlwt & argparse module & boto
sudo pip install xlwt
sudo pip install argparse
sudo pip install boto3
sudo pip install boto

## Running the Infrastructure Script on Terminal

Step 1 : You have to pass ACCESS_KEY,SECRET_KEY as parmaters of the particular IAM user preferrably with Administrator rights alongwith file & regions paramter

Step 2 : The file paramater will have the path of your local machine where you want to save the file and regions you have to specify all (every aws region) or specify regions separated by | (Ex: ap-south-1 | us-east-1 etc)

Step 3 : EX: python InfraSheets.py --file /Users/andmac37/Documents/AWS-Infrasheet-AutomationIntellyzenM.xls --access_key xyz --secret_key xyz --regions

Step 3 : The script shall prompt for yes/no everytime for adding the required sheet.If you choose yes the sheet will be added and if you choose no that sheet will not be visible in the excel file generated

Step 4 : After the exceution is completed the Infrastructure sheet will be downloaded on the path you had specified while running the script

## Running the Monitoring Script on Terminal       

Step 1 : You have to pass ACCESS_KEY,SECRET_KEY as parmaters of the particular IAM user preferrably with Administrator rights alongwith file & regions paramter

Step 2 : The file paramater will have the path of your local machine where you want to save the file and regions you have to specify all (every aws region) or specify regions separated by | (Ex: ap-south-1 | us-east-1 etc)

Step 3 : EX: python MonitoringSheet.py --file /Users/andmac37/Documents/AWS-Infrasheet-AutomationIntellyzenM.xls --access_key xyz --secret_key xyz --regions

Step 3 : The script shall prompt for yes/no everytime for adding the required sheet.If you choose yes the sheet will be added and if you choose no that sheet will not be visible in the excel file generated

Step 4 : After the exceution is completed the Infrastructure sheet will be downloaded on the path you had specified while running the script

## Built With

* [Pycharm](http://www.dropwizard.io/1.0.2/docs/) - The framework used
* [Boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) - Python SDK
* [XLWT](pip install xlwt) - This is a library for developers to use to generate spreadsheet files compatible with Microsoft Excel versions 95 to 2003
* [argparse](pip install argparse) - The argparse module makes it easy to write user-friendly command-line interfaces


<!--## Contributing-->

<!--Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.-->

<!--## Versioning-->

<!--We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). -->

## Authors

* **Siddhartha Sarnobat** - *Initial work*

<!--See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.-->

## License

This project is licensed under the IntellyZen License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

Thanks to the IntellyZen team for the patience with the Author

