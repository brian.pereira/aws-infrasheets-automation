
import boto3
import os
import sys
import argparse
import xlwt
from xlwt import Workbook
import boto

ACCESS_KEY = None
SECRET_KEY = None
regions = None


#regions = 'ap-southeast-1|us-east-2|us-east-1|us-west-1|us-west-2|ap-south-1|ap-northeast-2|ap-southeast-2|ap-northeast-1|ca-central-1|eu-central-1|eu-west-1|eu-west-2'

#filepath='/Users/andmac37/Desktop/ketto.xls'
#filepath2='/Users/andmac37/Documents/AWS-Infrasheet-Automation/ketto-Alarms.xls'

wb = Workbook()
wb2 = Workbook()


def open_file(filepath,sheetname):
  """
  Opens the output files, promts whether to overwrite
  """
  goaheadandopen = True
  if os.path.exists(filepath):
    if os.path.isfile(filepath):
      valid = {'yes': True, 'y': True,
               'no': False, 'n': False}

      while True:
        #sys.stdout.write('file %s exists, overwrite it? [y/n] ' % filepath)
        sys.stdout.write('Add sheet %s ? [y/n] ' % sheetname)
        choice = raw_input().lower()
        if choice in valid.keys():
          if not valid[choice]:
            goaheadandopen = False
          break
        sys.stdout.write('Please respond with \'yes\' or \'no\' (or \'y\' or \'n\').\n')
    else:  # folder
      sys.stdout.write('%s exists but nt a regular file. Aborting...\n')
      goaheadandopen = False

  if not goaheadandopen:
    return None

  try:
    f = file(filepath, 'wt')
  except Exception, e:
    f = None
    sys.stderr.write('Could not open file %s. reason: %s\n' % (filepath, e))

  return f


def ec2_connect(access_key, secret_key, regions):
  """
  Connects to EC2, returns a connection object
  """

  try:
    conn = ec2.connect_to_region(regions,
                                 aws_access_key_id=access_key,
                                 aws_secret_access_key=secret_key)
  except Exception, e:
    sys.stderr.write('Could not connect to region: %s. Exception: %s\n' % (regions, e))
    conn = None

  return conn


def ebs_details ():

  # opens file
  f = open_file(filepath,"EBS Details")
  if not f:
    return False
  region_list = regions.split('|')


  sheet2 = wb.add_sheet('EBS')
  style = xlwt.easyxf('font: bold 1,color red;')

  sheet2.write(0, 0, 'Sr.No', style)
  sheet2.write(0, 1, 'Region', style)
  sheet2.write(0, 2, 'AZ', style)
  sheet2.write(0, 3, 'Volume ID', style)
  sheet2.write(0, 4, 'Snapshot Id', style)
  sheet2.write(0, 5, 'Volume Type', style)
  sheet2.write(0, 6, 'IOPS', style)
  sheet2.write(0, 7, 'Size(GiB)', style)
  sheet2.write(0, 8, 'Attached to InstanceId', style)
  sheet2.write(0, 9, 'Attached to InstanceId State', style)
  sheet2.write(0, 10, 'Device', style)
  sheet2.write(0, 11, 'Encrypted', style)
  sheet2.write(0, 12, 'State', style)

  i=1
  j=0
  # go over all regions in list
  for region in region_list:

    #conn = ec2_connect(ACCESS_KEY,SECRET_KEY, region)
    client = boto3.client('ec2',
                          aws_access_key_id=ACCESS_KEY,
                          aws_secret_access_key=SECRET_KEY,
                          region_name=region
                          )

    paginator = client.get_paginator('describe_volumes')

    page_iterator = paginator.paginate()

    for page in page_iterator:

          for ebs_det in page['Volumes']:

              sheet2.write(i, j, i)
              j = j + 1
              sheet2.write(i, j, region)
              j = j + 1
              sheet2.write(i, j, ebs_det['AvailabilityZone'])
              j = j + 1
              sheet2.write(i, j, ebs_det['VolumeId'])
              j = j + 1
              sheet2.write(i, j, ebs_det['SnapshotId'])
              j = j + 1
              sheet2.write(i, j, ebs_det['VolumeType'])
              j = j + 1
              if ('Iops' not in ebs_det):
                  iops = ""
              else:
                  iops = ebs_det['Iops']
              sheet2.write(i, j, iops)
              j = j + 1
              sheet2.write(i, j, ebs_det['Size'])
              j = j + 1

              for att_det in ebs_det['Attachments']:
                sheet2.write(i, j, att_det['InstanceId'])
                j = j + 1
                sheet2.write(i, j, att_det['State'])
                j = j + 1
                sheet2.write(i, j, att_det['Device'])
                j = j + 1

              sheet2.write(i, j, ebs_det['Encrypted'])
              j = j + 1
              sheet2.write(i, j, ebs_det['State'])

              i = i + 1
              j = 0


  wb.save(filepath)
  f.close()
  return True

def ec2_details ():

  # opens file
  f = open_file(filepath,"EC2 Details")
  if not f:
    return False
  region_list = regions.split('|')


  sheet1 = wb.add_sheet('EC2')
  style = xlwt.easyxf('font: bold 1,color red;')

  sheet1.write(0, 0, 'Sr.No', style)
  sheet1.write(0, 1, 'Region', style)
  sheet1.write(0, 2, 'Vpc Id', style)
  sheet1.write(0, 3, 'Subnet Id', style)
  sheet1.write(0, 4, 'Instance Id', style)
  sheet1.write(0, 5, 'Instance Type', style)
  sheet1.write(0, 6, 'AMI ID', style)
  sheet1.write(0, 7, 'Tags Key', style)
  sheet1.write(0, 8, 'Tags Value', style)
  sheet1.write(0, 9, 'Key Name', style)
  sheet1.write(0, 10, 'Platform', style)
  sheet1.write(0, 11, 'PublicIpAddress', style)
  sheet1.write(0, 12, 'PublicDnsName', style)
  sheet1.write(0, 13, 'PrivateIpAddress', style)
  sheet1.write(0, 14, 'PrivateDnsName', style)
  sheet1.write(0, 15, 'Security Group Name', style)
  sheet1.write(0, 16, 'Security Group Id', style)

  i=1
  j=0
  # go over all regions in list
  for region in region_list:

    #conn = ec2_connect(ACCESS_KEY,SECRET_KEY, region)
    client = boto3.client('ec2',
                          aws_access_key_id=ACCESS_KEY,
                          aws_secret_access_key=SECRET_KEY,
                          region_name=region
                          )

    paginator = client.get_paginator('describe_instances')

    page_iterator = paginator.paginate()

    for page in page_iterator:

          for ec2_info in page['Reservations']:
             for ec2_det in ec2_info['Instances']:
                sheet1.write(i, j, i)
                j = j + 1
                sheet1.write(i, j, region)
                j = j + 1
                sheet1.write(i, j, ec2_det['VpcId'])
                j = j + 1
                sheet1.write(i, j, ec2_det['SubnetId'])
                j = j + 1
                sheet1.write(i, j, ec2_det['InstanceId'])
                j = j + 1
                sheet1.write(i, j, ec2_det['InstanceType'])
                j = j + 1
                sheet1.write(i, j, ec2_det['ImageId'])
                j = j + 1
                list_key = []
                list_value = []
                list_gp_name = []
                list_gp_id = []
                if 'Tags' in ec2_det:
                    for tags in ec2_det['Tags']:
                      if tags > 0:

                          list_key.append(tags['Key'])
                          # list_key.split(",")
                          list_value.append(tags['Value'])
                          # list_value.split(",")

                list_key = ', '.join(list_key)
                list_value = ', '.join(list_value)

                sheet1.write(i, j, list_key)
                j = j + 1
                sheet1.write(i, j, list_value)
                j = j + 1
                sheet1.write(i, j, ec2_det['KeyName'])
                j = j + 1
                if ('Platform' not in ec2_det):
                    platform = "Linux"
                else:
                    platform = ec2_det['Platform']
                sheet1.write(i, j, platform)
                j = j + 1
                if ('PublicIpAddress' not in ec2_det):
                    publicIpAddress = ""
                else:
                    publicIpAddress = ec2_det['PublicIpAddress']
                sheet1.write(i, j, publicIpAddress)
                j = j + 1
                if ('PublicDnsName' not in ec2_det):
                    publicDnsName = ""
                else:
                    publicDnsName = ec2_det['PublicDnsName']
                sheet1.write(i, j, publicDnsName)
                j = j + 1
                sheet1.write(i, j, ec2_det['PrivateIpAddress'])
                j = j + 1
                sheet1.write(i, j, ec2_det['PrivateDnsName'])
                j = j + 1

                for x in ec2_det['SecurityGroups']:
                  if x > 0:

                    list_gp_name.append(x['GroupName'])
                    list_gp_id.append(x['GroupId'])

                gp_name = ', '.join(list_gp_name)
                gp_id = ', '.join(list_gp_id)

                sheet1.write(i, j, gp_name)
                j = j + 1
                sheet1.write(i, j, gp_id)

                i = i + 1
                j = 0


  wb.save(filepath)
  f.close()
  return True


def s3_details ():


    # opens file
    f = open_file(filepath,"S3 Details")
    if not f:
        return False

    client = boto3.client('s3',
                          aws_access_key_id=ACCESS_KEY,
                          aws_secret_access_key=SECRET_KEY,
                          )

    sheet3 = wb.add_sheet('S3')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet3.write(0, 0, 'Sr.No', style)
    sheet3.write(0, 1, 'Bucket Name', style)
    sheet3.write(0, 2, 'Created On', style)


    s3_connection = client.list_buckets()
    i = 1
    j = 0
    for bucket in s3_connection['Buckets']:

        try:
            name = bucket['Name'],
        except:
            name = u''
        try:
            created = str(bucket['CreationDate']),
        except:
            created = u''

        print("filling sheet...")
        sheet3.write(i, j, i)
        j = j + 1
        sheet3.write(i, j, name)
        j = j + 1
        sheet3.write(i, j, created)

        i = i + 1
        j = 0
        print i
        print j
        print("here")

    wb.save(filepath)
    f.close()
    return True

def elb_details():


    # opens file
    f = open_file(filepath,"ELB Details")
    if not f:
        return False

    region_list = regions.split('|')

    sheet15 = wb.add_sheet('Load Balancer')
    style = xlwt.easyxf('font: bold 1,color red;')

    sheet15.write(0, 0, 'Sr.No', style)
    sheet15.write(0, 1, 'Region', style)
    sheet15.write(0, 2, 'AZ(SubnetId->ZoneName)', style)
    sheet15.write(0, 3, 'LoadBalancer Name', style)
    sheet15.write(0, 4, 'DNS Name', style)
    sheet15.write(0, 5, 'State', style)
    sheet15.write(0, 6, 'LoadBalancer ARN', style)
    sheet15.write(0, 7, 'Type', style)
    sheet15.write(0, 8, 'VpcId', style)
    sheet15.write(0, 9, 'Scheme', style)
    sheet15.write(0, 10, 'Security Groups', style)
    i = 1
    j = 0
    for region in region_list:


        volume_dictELB = {}
        elbList = boto3.client('elbv2',aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY,region_name=region)

        paginator = elbList.get_paginator('describe_load_balancers')

        page_iterator = paginator.paginate()

        for page in page_iterator:

        #bals = elbList.describe_load_balancers()
            for elb in page['LoadBalancers']:
                list_key = []
                list_sec = []
                zet_key = []
                zet_final = ""
                zet_list = []
                for az in elb['AvailabilityZones']:
                    if az > 0:
                        list_key.append(az['SubnetId'])
                        #list_key.split(",")
                        list_key.append(az['ZoneName'])

                        zet_key = '-> '.join(list_key)
                        zet_list.append(zet_key)
                        list_key.remove(az['SubnetId'])
                        list_key.remove(az['ZoneName'])

                    else:
                        break

                zet_final = ','.join(zet_list)
                sheet15.write(i, j, i)
                j = j + 1
                sheet15.write(i, j, region)
                j = j + 1
                sheet15.write(i, j, zet_final)
                j = j + 1
                sheet15.write(i, j, elb['LoadBalancerName'])
                j = j + 1
                sheet15.write(i, j, elb['DNSName'])
                j = j + 1
                sheet15.write(i, j, elb['State']['Code'])
                j = j + 1
                sheet15.write(i, j, elb['LoadBalancerArn'])
                j = j + 1
                sheet15.write(i, j, elb['Type'])
                j = j + 1
                sheet15.write(i, j, elb['VpcId'])
                j = j + 1
                sheet15.write(i, j, elb['Scheme'])
                j = j + 1
                for sec in elb['SecurityGroups']:
                    if sec > 0:
                        list_sec.append(sec)
                        #list_key.split(",")
                    else:
                        break
                sheet15.write(i, j, list_sec)

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()
    return True

def elb_classic_details():


    # opens file
    f = open_file(filepath,"ELB Classic Details")
    if not f:
        return False

    region_list = regions.split('|')

    sheet16 = wb.add_sheet('Load Balancer Classic')
    style = xlwt.easyxf('font: bold 1,color red;')

    sheet16.write(0, 0, 'Sr.No', style)
    sheet16.write(0, 1, 'Region', style)
    sheet16.write(0, 2, 'AZ', style)
    sheet16.write(0, 3, 'LoadBalancer Name', style)
    sheet16.write(0, 4, 'DNS Name', style)
    sheet16.write(0, 5, 'CanonicalHostedZoneNameID', style)
    sheet16.write(0, 6, 'CanonicalHostedZoneName', style)
    sheet16.write(0, 7, 'VpcId', style)
    sheet16.write(0, 8, 'Scheme', style)
    sheet16.write(0, 9, 'Security Groups', style)
    i = 1
    j = 0
    for region in region_list:


        elbList = boto3.client('elb',aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY,region_name=region)

        paginator = elbList.get_paginator('describe_load_balancers')

        page_iterator = paginator.paginate()

        for page in page_iterator:
        #bals = elbList.describe_load_balancers()
            for elb in page['LoadBalancerDescriptions']:
                list_key = []
                list_sec = []
                zet_key = []
                zet_final = ""
                zet_list = []
                for az in elb['AvailabilityZones']:
                    list_key.append(az)

                #z_key = ', '.join(zet_key)
                #list_value = ', '.join(list_value)
                zet_final = ','.join(zet_list)
                sheet16.write(i, j, i)
                j = j + 1
                sheet16.write(i, j, region)
                j = j + 1
                sheet16.write(i, j, az)
                j = j + 1
                sheet16.write(i, j, elb['LoadBalancerName'])
                j = j + 1
                sheet16.write(i, j, elb['DNSName'])
                j = j + 1
                sheet16.write(i, j, elb['CanonicalHostedZoneNameID'])
                j = j + 1
                sheet16.write(i, j, elb['CanonicalHostedZoneName'])
                j = j + 1
                sheet16.write(i, j, elb['VPCId'])
                j = j + 1
                sheet16.write(i, j, elb['Scheme'])
                j = j + 1
                for sec in elb['SecurityGroups']:
                    if sec > 0:
                        list_sec.append(sec)
                        #list_key.split(",")
                    else:
                        break
                sheet16.write(i, j, list_sec)

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()
    return True

def iam_details ():


    # opens file
    f = open_file(filepath,"IAM Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet6 = wb.add_sheet('IAM')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet6.write(0, 0, 'Sr.No', style)
    sheet6.write(0, 1, 'Username', style)
    sheet6.write(0, 2, 'Groups', style)
    sheet6.write(0, 3, 'Policies', style)
    sheet6.write(0, 4, 'isMFADeviceConfigured', style)

    client = boto3.client('iam', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
    users = client.list_users()
    user_list = []

    paginator = client.get_paginator('list_users')

    page_iterator = paginator.paginate()

    for page in page_iterator:

        for key in page['Users']:
            result = {}
            Policies = []
            Groups = []

            result['userName'] = key['UserName']
            List_of_Policies = client.list_user_policies(UserName=key['UserName'])

            #result['Policies'] = List_of_Policies['PolicyNames']
            #result['Policies'] = Policies

            for PolicyExt in List_of_Policies['PolicyNames']:
                Policies.append(PolicyExt)
            result['Policies'] = Policies

            List_of_Groups = client.list_groups_for_user(UserName=key['UserName'])

            for Group in List_of_Groups['Groups']:
                Groups.append(Group['GroupName'])
            result['Groups'] = Groups

            List_of_MFA_Devices = client.list_mfa_devices(UserName=key['UserName'])

            if not len(List_of_MFA_Devices['MFADevices']):
                result['isMFADeviceConfigured'] = False
            else:
                result['isMFADeviceConfigured'] = True
            user_list.append(result)


    for key in user_list:

        print key
        sheet6.write(i, j, i)
        j = j + 1
        sheet6.write(i, j, key['userName'])
        j = j + 1
        sheet6.write(i, j, key['Groups'])
        j = j + 1
        sheet6.write(i, j, key['Policies'])
        j = j + 1
        sheet6.write(i, j, key['isMFADeviceConfigured'])

        i = i + 1
        j = 0
        print i
        print j
        print("here")
    wb.save(filepath)
    f.close()
    return True

def cloudfront_details ():


    # opens file
    f = open_file(filepath,"CloudFront Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet7 = wb.add_sheet('CloudFront')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet7.write(0, 0, 'Sr.No', style)
    sheet7.write(0, 1, 'Distribution ID', style)
    sheet7.write(0, 2, 'ARN', style)
    sheet7.write(0, 3, 'Domain Name', style)
    sheet7.write(0, 4, 'Origin Domain Name & Path', style)
    sheet7.write(0, 5, 'Origin ID', style)
    sheet7.write(0, 6, 'Status', style)
    sheet7.write(0, 7, 'State', style)
   # sheet7.write(0, 8, 'Aliases', style)


    cf = boto3.client('cloudfront', aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)

    paginator = cf.get_paginator('list_distributions')

    page_iterator = paginator.paginate()

    for distributions in page_iterator:

        #distributions = cf.list_distributions()
        if distributions['DistributionList']['Quantity'] > 0:
            for distribution in distributions['DistributionList']['Items']:
                print("Domain: " + distribution['DomainName'])
                print("Distribution Id: " + distribution['Id'])
                print("Certificate Source: " + distribution['ViewerCertificate']['CertificateSource'])
                if (distribution['ViewerCertificate']['CertificateSource'] == "acm"):
                    print("Certificate: " + distribution['ViewerCertificate']['Certificate'])
                sheet7.write(i, j, i)
                j = j + 1
                sheet7.write(i, j, distribution['Id'])
                j = j + 1
                sheet7.write(i, j, distribution['ARN'])
                j = j + 1
                sheet7.write(i, j, distribution['DomainName'])
                j = j + 1
                # sheet4.write(i, j, ','.join(rds['allocated_storage'] for allocated_storage in rds))
                # j = j + 1
                list_key = []
                list_value = []
                for origin in distribution['Origins']['Items']:
                    if origin > 0:
                        list_key.append(origin['DomainName'])
                        # list_key.split(",")
                        list_value.append(origin['Id'])
                         # list_value.split(",")
                    else:
                        break

                list_key = ', '.join(list_key)
                list_value = ', '.join(list_value)
                sheet7.write(i, j, list_key)
                j = j + 1
                sheet7.write(i, j, list_value)
                j = j + 1
                sheet7.write(i, j, distribution['Status'])
                j = j + 1
                sheet7.write(i, j, distribution['Enabled'])
                i = i + 1
                j = 0
                print i
                print j
                print("here")

        else:
            print("Error - No CloudFront Distributions Detected.")

    wb.save(filepath)
    f.close()
    return True

def dynamodb_details ():


    # opens file
    f = open_file(filepath,"DynamoDB Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet11 = wb.add_sheet('DynamoDB')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet11.write(0, 0, 'Sr.No', style)
    sheet11.write(0, 1, 'Region', style)
    sheet11.write(0, 2, 'Table Name', style)
    sheet11.write(0, 3, 'Table Size Bytes', style)
    sheet11.write(0, 4, 'Item Count', style)
    sheet11.write(0, 5, 'Table Id', style)
    sheet11.write(0, 6, 'Table ARN', style)
    sheet11.write(0, 7, 'Table Status', style)
    sheet11.write(0, 8, 'Read Capacity Units', style)
    sheet11.write(0, 9, 'Write Capacity Units', style)

    #region = 'eu-central-1'

    region_list = regions.split('|')

    # go over all regions in list
    for region in region_list:
        client = boto3.client('dynamodb',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region
                              )
        paginator = client.get_paginator('list_tables')

        page_iterator = paginator.paginate()

        for page in page_iterator:

            for table_name in page['TableNames']:

                table_info = client.describe_table(TableName=table_name)
                table_details = table_info['Table']
                print table_info['Table']
                print table_info['Table']['TableArn']
                sheet11.write(i, j, i)
                j = j + 1
                sheet11.write(i, j, region)
                j = j + 1
                sheet11.write(i, j, table_details['TableName'])
                j = j + 1
                sheet11.write(i, j, table_details['TableSizeBytes'])
                j = j + 1
                sheet11.write(i, j, table_details['ItemCount'])
                j = j + 1
                sheet11.write(i, j, table_details['TableId'])
                j = j + 1
                sheet11.write(i, j, table_details['TableArn'])
                j = j + 1
                sheet11.write(i, j, table_details['TableStatus'])
                j = j + 1
                sheet11.write(i, j, table_details['ProvisionedThroughput']['ReadCapacityUnits'])
                j = j + 1
                sheet11.write(i, j, table_details['ProvisionedThroughput']['WriteCapacityUnits'])

                i= i + 1
                j = 0

    wb.save(filepath)
    f.close()
    return True

def nat_details ():
    # opens file
    f = open_file(filepath,"NAT Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet8 = wb.add_sheet('NAT Gateway')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet8.write(0, 0, 'Sr.No', style)
    sheet8.write(0, 1, 'Region', style)
    sheet8.write(0, 2, 'Tags Key', style)
    sheet8.write(0, 3, 'Tags Value', style)
    sheet8.write(0, 4, 'NAT Gateway Id', style)
    sheet8.write(0, 5, 'VPC ID', style)
    sheet8.write(0, 6, 'SUBNET ID', style)
    sheet8.write(0, 7, 'Public IP', style)
    sheet8.write(0, 8, 'Private IP', style)
    sheet8.write(0, 9, 'Network Interface Id', style)
    sheet8.write(0, 10, 'State', style)

    #region = 'eu-west-2'

    region_list = regions.split('|')

    # go over all regions in list
    for region in region_list:
        client = boto3.client('ec2',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region
                              )

        paginator = client.get_paginator('describe_nat_gateways')

        page_iterator = paginator.paginate()

        for nat_info in page_iterator:

            for nat_det in nat_info['NatGateways']:

                sheet8.write(i, j, i)
                j = j + 1
                sheet8.write(i, j, region)
                j = j + 1
                list_key = []
                list_value = []
                for tags in nat_det['Tags']:
                    if tags > 0:

                        list_key.append(tags['Key'])
                        #list_key.split(",")
                        list_value.append(tags['Value'])
                        #list_value.split(",")
                    else:
                        break

                list_key = ', '.join(list_key)
                list_value = ', '.join(list_value)

                sheet8.write(i, j, list_key)
                j = j + 1
                sheet8.write(i, j, list_value)
                j = j + 1
                sheet8.write(i, j, nat_det['NatGatewayId'])
                j = j + 1
                sheet8.write(i, j, nat_det['VpcId'])
                j = j + 1
                sheet8.write(i, j, nat_det['SubnetId'])
                j = j + 1

                for nat_add in nat_det['NatGatewayAddresses']:
                    sheet8.write(i, j, nat_add['PublicIp'])
                    j = j + 1
                    sheet8.write(i, j, nat_add['PrivateIp'])
                    j = j + 1
                    sheet8.write(i, j, nat_add['NetworkInterfaceId'])
                    j = j + 1

                sheet8.write(i, j, nat_det['State'])

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()
    return True

def security_group_details ():
    # opens file
    f = open_file(filepath,"Security Group Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet10 = wb.add_sheet('Security Groups')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet10.write(0, 0, 'Sr.No', style)
    sheet10.write(0, 1, 'Region', style)
    sheet10.write(0, 2, 'GroupName', style)
    sheet10.write(0, 3, 'GroupId', style)
    sheet10.write(0, 4, 'Description', style)
    sheet10.write(0, 5, 'VpcId', style)

    #region = 'ap-southeast-1'

    region_list = regions.split('|')

    # go over all regions in list
    for region in region_list:
        client = boto3.client('ec2',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region
                              )

        # tables = dynamodb.list_tables()
        #sec_info = client.describe_security_groups()

        paginator = client.get_paginator('describe_security_groups')

        page_iterator = paginator.paginate()

        for page in page_iterator:

            for sec_det in page['SecurityGroups']:

                sheet10.write(i, j, i)
                j = j + 1
                sheet10.write(i, j, region)
                j = j + 1
                sheet10.write(i, j, sec_det['GroupName'])
                j = j + 1
                sheet10.write(i, j, sec_det['GroupId'])
                j = j + 1
                sheet10.write(i, j, sec_det['Description'])
                j = j + 1
                if 'VpcId' in sec_det:
                    sheet10.write(i, j, sec_det['VpcId'])

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()
    return True


def lambda_details ():
    # opens file
    f = open_file(filepath,"Lambda Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet12 = wb.add_sheet('Lambda Details')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet12.write(0, 0, 'Sr.No', style)
    sheet12.write(0, 1, 'Region', style)
    sheet12.write(0, 2, 'Function Name', style)
    sheet12.write(0, 3, 'Function ARN', style)
    sheet12.write(0, 4, 'Description', style)
    sheet12.write(0, 5, 'Timeout', style)
    sheet12.write(0, 6, 'Memory Size', style)
    sheet12.write(0, 7, 'Code Size', style)
    sheet12.write(0, 8, 'Version', style)
    sheet12.write(0, 9, 'Role', style)
    sheet12.write(0, 10, 'Runtime', style)

    #region = 'ap-southeast-1'

    region_list = regions.split('|')

    # go over all regions in list
    for region in region_list:
        client = boto3.client('lambda',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region
                              )

        paginator = client.get_paginator('list_functions')

        page_iterator = paginator.paginate()

        for page in page_iterator:

            for lambda_det in page['Functions']:

                sheet12.write(i, j, i)
                j = j + 1
                sheet12.write(i, j, region)
                j = j + 1
                sheet12.write(i, j, lambda_det['FunctionName'])
                j = j + 1
                sheet12.write(i, j, lambda_det['FunctionArn'])
                j = j + 1
                sheet12.write(i, j, lambda_det['Description'])
                j = j + 1
                sheet12.write(i, j, lambda_det['Timeout'])
                j = j + 1
                sheet12.write(i, j, lambda_det['MemorySize'])
                j = j + 1
                sheet12.write(i, j, lambda_det['CodeSize'])
                j = j + 1
                sheet12.write(i, j, lambda_det['Version'])
                j = j + 1
                sheet12.write(i, j, lambda_det['Role'])
                j = j + 1
                sheet12.write(i, j, lambda_det['Runtime'])

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()
    return True

def cloudtrail_details ():
    # opens file
    f = open_file(filepath,"CloudTrail Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet9 = wb.add_sheet('CloudTrail Details')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet9.write(0, 0, 'Sr.No', style)
    sheet9.write(0, 1, 'Region', style)
    sheet9.write(0, 2, 'Name', style)
    sheet9.write(0, 3, 'S3BucketName', style)
    sheet9.write(0, 4, 'CloudWatchLogsGroup', style)
    sheet9.write(0, 5, 'CloudWatchLogsRole', style)
    sheet9.write(0, 6, 'TrailARN', style)
    sheet9.write(0, 7, 'IsMultiRegionTrail', style)
    sheet9.write(0, 8, 'HomeRegion', style)

    region_list = regions.split('|')

    # go over all regions in list
    for region in region_list:
        client = boto3.client('cloudtrail',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region
                              )

        # tables = dynamodb.list_tables()
        trail_info = client.describe_trails()

        for trail_det in trail_info['trailList']:

            sheet9.write(i, j, i)
            j = j + 1
            sheet9.write(i, j, region)
            j = j + 1
            sheet9.write(i, j, trail_det['Name'])
            j = j + 1
            sheet9.write(i, j, trail_det['S3BucketName'])
            j = j + 1
            sheet9.write(i, j, trail_det['CloudWatchLogsLogGroupArn'])
            j = j + 1
            sheet9.write(i, j, trail_det['CloudWatchLogsRoleArn'])
            j = j + 1
            sheet9.write(i, j, trail_det['TrailARN'])
            j = j + 1
            sheet9.write(i, j, trail_det['IsMultiRegionTrail'])
            j = j + 1
            sheet9.write(i, j, trail_det['HomeRegion'])

            i = i + 1
            j = 0

    wb.save(filepath)
    f.close()
    return True

def route53_details ():
    # opens file
    f = open_file(filepath,"Route 53 Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet13 = wb.add_sheet('Route53 Details')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet13.write(0, 0, 'Sr.No', style)
    sheet13.write(0, 1, 'Id', style)
    sheet13.write(0, 2, 'Name', style)
    sheet13.write(0, 3, 'Private Zone', style)

    #region = 'ap-southeast-1'

    #region_list = regions.split('|')

    # go over all regions in list
    client = boto3.client('route53',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY
                              )

     # tables = dynamodb.list_tables()

    paginator = client.get_paginator('list_hosted_zones')

    page_iterator = paginator.paginate()

    for page in page_iterator:

        # for table in tables["TableNames"]:
        # print "tables" + existing_tables["TableName"]

        for route53_det in page['HostedZones']:

            sheet13.write(i, j, i)
            j = j + 1
            sheet13.write(i, j, route53_det['Id'])
            j = j + 1
            sheet13.write(i, j, route53_det['Name'])
            j = j + 1
            sheet13.write(i, j, route53_det['Config']['PrivateZone'])
    #        j = j + 1
    #        sheet13.write(i, j, route53_det['Config']['Comment'])

            i = i + 1
            j = 0

    wb.save(filepath)
    f.close()

    return True

def sns_details ():
    # opens file
    f = open_file(filepath,"SNS Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet14 = wb.add_sheet('SNS Details')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet14.write(0, 0, 'Sr.No', style)
    sheet14.write(0, 1, 'TopicArn', style)
    sheet14.write(0, 2, 'SubsriptionArn', style)
    sheet14.write(0, 3, 'Protocol', style)
    sheet14.write(0, 4, 'Endpoint', style)
    sheet14.write(0, 5, 'Owner', style)

    #region = 'ap-southeast-1'

    region_list = regions.split('|')
    # go over all regions in list
    for region in region_list:

        # go over all regions in list
        client = boto3.client('sns',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region)

        paginator = client.get_paginator('list_subscriptions')

        page_iterator = paginator.paginate()

        for page in page_iterator:

        # tables = dynamodb.list_tables()
        #sns_info = client.list_subscriptions()

        # for table in tables["TableNames"]:
        # print "tables" + existing_tables["TableName"]

            for sns_det in page['Subscriptions']:

                sheet14.write(i, j, i)
                j = j + 1
                sheet14.write(i, j, sns_det['TopicArn'])
                j = j + 1
                sheet14.write(i, j, sns_det['SubscriptionArn'])
                j = j + 1
                sheet14.write(i, j, sns_det['Protocol'])
                j = j + 1
                sheet14.write(i, j, sns_det['Endpoint'])
                j = j + 1
                sheet14.write(i, j, sns_det['Owner'])

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()

    return True

def sqs_details ():
    # opens file
    f = open_file(filepath,"SQS Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet14 = wb.add_sheet('SQS Details')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet14.write(0, 0, 'Sr.No', style)
    sheet14.write(0, 1, 'Region', style)
    sheet14.write(0, 2, 'Queue URL', style)

    #region = 'ap-southeast-1'

    region_list = regions.split('|')
    # go over all regions in list
    for region in region_list:

        # go over all regions in list
        client = boto3.client('sqs',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region)


        # tables = dynamodb.list_tables()
        sqs_info = client.list_queues()
        if 'QueueUrls' not in sqs_info:
            continue
        # for table in tables["TableNames"]:
        # print "tables" + existing_tables["TableName"]

        for sqs_det in sqs_info['QueueUrls']:

            sheet14.write(i, j, i)
            j = j + 1
            sheet14.write(i, j, region)
            j = j + 1
            sheet14.write(i, j, sqs_det)

            i = i + 1
            j = 0

    wb.save(filepath)
    f.close()

    return True

def api_gateway_details ():
    # opens file
    f = open_file(filepath,"API Gateway Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet17 = wb.add_sheet('API Gateway Details')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet17.write(0, 0, 'Sr.No', style)
    sheet17.write(0, 1, 'Id', style)
    sheet17.write(0, 2, 'Name', style)
    sheet17.write(0, 3, 'Description', style)
    sheet17.write(0, 4, 'Endpoint', style)
    sheet17.write(0, 5, 'Region', style)
    sheet17.write(0, 6, 'Created Date', style)

    #region = 'ap-southeast-1'

    #region_list = regions.split('|')
    region_list = regions.split('|')
    # go over all regions in list
    for region in region_list:

    # go over all regions in list
        client = boto3.client('apigateway',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region)


        paginator = client.get_paginator('get_rest_apis')

        page_iterator = paginator.paginate()

        for page in page_iterator:

            for api_det in page['items']:

                sheet17.write(i, j, i)
                j = j + 1
                sheet17.write(i, j, api_det['id'])
                j = j + 1
                sheet17.write(i, j, api_det['name'])
                j = j + 1
                if 'description' not in api_det:
                    j = j + 1
                else:
                    sheet17.write(i, j, api_det['description'])
                    j = j + 1
                for endpoint in api_det['endpointConfiguration']['types']:
                    sheet17.write(i, j, endpoint)
                    j = j + 1

                sheet17.write(i, j, region)
                j = j + 1
                sheet17.write(i, j, str(api_det['createdDate']))

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()

    return True

def redshift_details ():
    # opens file
    f = open_file(filepath,"Redshift Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet14 = wb.add_sheet('Redshift Details')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet14.write(0, 0, 'Sr.No', style)
    sheet14.write(0, 1, 'Region', style)
    sheet14.write(0, 2, 'ClusterIdentifier', style)
    sheet14.write(0, 3, 'NodeType', style)
    sheet14.write(0, 4, 'ClusterStatus', style)
    #sheet14.write(0, 5, 'ModifyStatus', style)
    sheet14.write(0, 5, 'MasterUsername', style)
    sheet14.write(0, 6, 'DBName', style)
    sheet14.write(0, 7, 'Endpoint Address', style)
    sheet14.write(0, 8, 'Endpoint Port', style)
    sheet14.write(0, 9, 'AutomatedSnapshotRetentionPeriod', style)
    sheet14.write(0, 10, 'ManualSnapshotRetentionPeriod', style)
    sheet14.write(0, 11, 'ClusterSubnetGroupName', style)
    #sheet14.write(0, 12, 'ClusterSecurityGroupName', style)
    #sheet14.write(0, 13, 'ClusterSecurityGroupStatus', style)
    sheet14.write(0, 12, 'VpcId', style)
    sheet14.write(0, 13, 'AvailabilityZone', style)
    sheet14.write(0, 14, 'PreferredMaintenanceWindow', style)
    sheet14.write(0, 15, 'ClusterVersion', style)
    sheet14.write(0, 16, 'AllowVersionUpgrade', style)
    sheet14.write(0, 17,  'NumberOfNodes', style)
    sheet14.write(0, 18, 'PubliclyAccessible', style)
    sheet14.write(0, 19, 'Encrypted', style)
    sheet14.write(0, 20, 'ClusterPublicKey', style)

    #region = 'ap-southeast-1'

    region_list = regions.split('|')
    # go over all regions in list
    for region in region_list:

        # go over all regions in list
        client = boto3.client('redshift',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region)

        # tables = dynamodb.list_tables()
        #redshift_info = client.describe_clusters()

        paginator = client.get_paginator('describe_clusters')

        page_iterator = paginator.paginate()

        for page in page_iterator:

        # for table in tables["TableNames"]:
        # print "tables" + existing_tables["TableName"]

            for redshift_det in page['Clusters']:

                sheet14.write(i, j, i)
                j = j + 1
                sheet14.write(i, j, region)
                j = j + 1
                sheet14.write(i, j, redshift_det['ClusterIdentifier'])
                j = j + 1
                sheet14.write(i, j, redshift_det['NodeType'])
                j = j + 1
                sheet14.write(i, j, redshift_det['ClusterStatus'])
                j = j + 1
                #sheet14.write(i, j, redshift_det['ModifyStatus'])
                #j = j + 1
                sheet14.write(i, j, redshift_det['MasterUsername'])
                j = j + 1
                if 'DBName' not in redshift_det:
                    j= j + 1
                else:
                    sheet14.write(i, j, redshift_det['DBName'])
                    j = j + 1
                sheet14.write(i, j, redshift_det['Endpoint']['Address'])
                j = j + 1
                sheet14.write(i, j, redshift_det['Endpoint']['Port'])
                j = j + 1
                sheet14.write(i, j, redshift_det['AutomatedSnapshotRetentionPeriod'])
                j = j + 1
                sheet14.write(i, j, redshift_det['ManualSnapshotRetentionPeriod'])
                j = j + 1

                sheet14.write(i, j, redshift_det['ClusterSubnetGroupName'])
                j = j + 1
                sheet14.write(i, j, redshift_det['VpcId'])
                j = j + 1
                sheet14.write(i, j, redshift_det['AvailabilityZone'])
                j = j + 1
                sheet14.write(i, j, redshift_det['PreferredMaintenanceWindow'])
                j = j + 1
                sheet14.write(i, j, redshift_det['ClusterVersion'])
                j = j + 1
                sheet14.write(i, j, redshift_det['AllowVersionUpgrade'])
                j = j + 1
                sheet14.write(i, j, redshift_det['NumberOfNodes'])
                j = j + 1
                sheet14.write(i, j, redshift_det['PubliclyAccessible'])
                j = j + 1
                sheet14.write(i, j, redshift_det['Encrypted'])
                j = j + 1
                sheet14.write(i, j, redshift_det['ClusterPublicKey'])

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()

    return True


def acm_details ():
    # opens file
    f = open_file(filepath,"ACM Details")
    if not f:
        return False

    i = 1
    j = 0

    sheet14 = wb.add_sheet('ACM Details')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet14.write(0, 0, 'Sr.No', style)
    sheet14.write(0, 1, 'Region', style)
    sheet14.write(0, 2, 'CertificationARN', style)
    sheet14.write(0, 3, 'Domain Name', style)
    sheet14.write(0, 4, 'SubjectAlternativeNames', style)
    sheet14.write(0, 5, 'CreatedAt', style)
    sheet14.write(0, 6, 'IssuedAt', style)
    sheet14.write(0, 7, 'ImportedAt', style)
    sheet14.write(0, 8, 'Status', style)
    sheet14.write(0, 9, 'NotBefore', style)
    sheet14.write(0, 10, 'NotAfter', style)
    sheet14.write(0, 11, 'RenewalEligibility', style)
    sheet14.write(0, 12, 'InUseBy', style)

    #region = 'ap-southeast-1'

    region_list = regions.split('|')
    # go over all regions in list
    for region in region_list:

        # go over all regions in list
        client = boto3.client('acm',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region)

        # tables = dynamodb.list_tables()
        #acm_info = client.list_certificates()

        paginator = client.get_paginator('list_certificates')

        # print(list_metrics_info['NextToken'])

        page_iterator = paginator.paginate()

        for page in page_iterator:

            for acm_det in page['CertificateSummaryList']:

                sheet14.write(i, j, i)
                j = j + 1
                sheet14.write(i, j, region)
                j = j + 1
                sheet14.write(i, j, acm_det['CertificateArn'])
                print acm_det['CertificateArn']
                j = j + 1

                cert_desc_info = client.describe_certificate(CertificateArn=acm_det['CertificateArn'])
                cert_desc = cert_desc_info['Certificate']

                sheet14.write(i, j, cert_desc['DomainName'])
                j = j + 1
                sheet14.write(i, j, cert_desc['SubjectAlternativeNames'])
                j = j + 1
                if 'CreatedAt' not in cert_desc:
                    j = j + 1
                else:
                    sheet14.write(i, j, str(cert_desc['CreatedAt']))
                    j = j + 1
                #(str(cert_desc['CreatedAt']))
                if 'IssuedAt' not in cert_desc:
                    j = j + 1
                else:
                    sheet14.write(i, j, str(cert_desc['IssuedAt']))
                    j = j + 1

                if 'ImportedAt' not in cert_desc:
                    j = j + 1
                else:
                    sheet14.write(i, j, str(cert_desc['ImportedAt']))
                    j = j + 1

                sheet14.write(i, j, cert_desc['Status'])
                j = j + 1

                if 'NotBefore' not in cert_desc:
                    j = j + 1
                else:
                    sheet14.write(i, j, str(cert_desc['NotBefore']))
                    j = j + 1

                if 'NotAfter' not in cert_desc:
                    j = j + 1
                else:
                    sheet14.write(i, j, str(cert_desc['NotAfter']))
                    j = j + 1

                sheet14.write(i, j, cert_desc['RenewalEligibility'])
                j = j + 1
                sheet14.write(i, j, cert_desc['InUseBy'])

                i = i + 1
                j = 0

    wb.save(filepath)
    f.close()

    return True

# def list_alarms ():
#     # opens file
#     f = open_file(filepath2,"List Alarms")
#     if not f:
#         return False
#
#     i = 1
#     j = 0
#
#     sheet1 = wb2.add_sheet('List Alarms')
#     style = xlwt.easyxf('font: bold 1,color red;')
#     sheet1.write(0, 0, 'Sr.No', style)
#     sheet1.write(0, 1, 'Region', style)
#     sheet1.write(0, 2, 'Alarm Name', style)
#     sheet1.write(0, 3, 'Alarm Description', style)
#     sheet1.write(0, 4, 'Evaluation Periods', style)
#     sheet1.write(0, 5, 'Comparison Operator', style)
#     sheet1.write(0, 6, 'Datapoints To Alarm', style)
#     sheet1.write(0, 7, 'Threshold', style)
#     sheet1.write(0, 8, 'Period', style)
#     sheet1.write(0, 9, 'Metric Name', style)
#     sheet1.write(0, 10, 'Namespace', style)
#     sheet1.write(0, 11, 'Statistic', style)
#     sheet1.write(0, 12, 'State Value', style)
#     sheet1.write(0, 13, 'State Reason', style)
#     sheet1.write(0, 14, 'Alarm ARN', style)
#
#     #region = 'ap-southeast-1'
#
#     region_list = regions.split('|')
#
#     # go over all regions in list
#     for region in region_list:
#         print region_list[0]
#         client = boto3.client('cloudwatch',
#                               aws_access_key_id=ACCESS_KEY,
#                               aws_secret_access_key=SECRET_KEY,
#                               region_name=region
#                               )
#
#         # Create a reusable Paginator
#         paginator = client.get_paginator('describe_alarms')
#
#         # tables = dynamodb.list_tables()
#         list_metrics_info = client.describe_alarms()
#
#         #print(list_metrics_info['NextToken'])
#
#         page_iterator = paginator.paginate()
#
#         for page in page_iterator:
#             #print(page['Contents'])
#         # for table in tables["TableNames"]:
#         # print "tables" + existing_tables["TableName"]
#
#             for metrics_info in page['MetricAlarms']:
#
#                 sheet1.write(i, j, i)
#                 j = j + 1
#                 sheet1.write(i, j, region)
#                 j = j + 1
#                 sheet1.write(i, j, metrics_info['AlarmName'])
#                 j = j + 1
#                 if 'AlarmDescription' not in metrics_info:
#                     j = j + 1
#                 else:
#                     sheet1.write(i, j, metrics_info['AlarmDescription'])
#                     j = j + 1
#                 sheet1.write(i, j, metrics_info['EvaluationPeriods'])
#                 j = j + 1
#                 sheet1.write(i, j, metrics_info['ComparisonOperator'])
#                 j = j + 1
#
#            # if metrics_info['Namespace'] in ['AWS/EC2','CWAgent'] and metrics_info['MetricName'] != 'CPUUtilization' or metrics_info['MetricName'] != 'StatusCheckFailed' :
#                 if metrics_info['Namespace'] in ['AWS/EC2','CWAgent'] and metrics_info['MetricName'] not in ['CPUUtilization','StatusCheckFailed','StatusCheckFailed_Instance','StatusCheckFailed_System']:
#                     sheet1.write(i, j, metrics_info['DatapointsToAlarm'])
#                     j = j + 1
#
#                 else:
#                     sheet1.write(i, j, "NULL")
#                     j = j + 1
#
#                 sheet1.write(i, j, metrics_info['Threshold'])
#                 j = j + 1
#                 sheet1.write(i, j, metrics_info['Period'])
#                 j = j + 1
#                 sheet1.write(i, j, metrics_info['MetricName'])
#                 j = j + 1
#                 sheet1.write(i, j, metrics_info['Namespace'])
#                 j = j + 1
#                 sheet1.write(i, j, metrics_info['Statistic'])
#                 j = j + 1
#                 sheet1.write(i, j, metrics_info['StateValue'])
#                 j = j + 1
#                 sheet1.write(i, j, metrics_info['StateReason'])
#                 j = j + 1
#                 sheet1.write(i, j, metrics_info['AlarmArn'])
#
#                 i = i + 1
#                 j = 0
#
#     wb2.save(filepath2)
#     f.close()
#     return True
#




def redis_details():
    # opens file
    f = open_file(filepath, "REDIS Details")
    if not f:
        return False

    i = 1
    j = 0
    # Uncomment if your region is different from regions defined at the starting
    # regions = 'ap-south-1|eu-central-1'

    sheet5 = wb.add_sheet('Redis')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet5.write(0, 0, 'Sr.No', style)
    sheet5.write(0, 1, 'Region', style)
    sheet5.write(0, 2, 'Name', style)
    sheet5.write(0, 3, 'Status', style)
    sheet5.write(0, 4, 'Node Type', style)
    sheet5.write(0, 5, 'Engine', style)
    sheet5.write(0, 6, 'Engine Version Capability', style)
    sheet5.write(0, 7, 'Availability Zone', style)
    sheet5.write(0, 8, 'No of Nodes', style)
    sheet5.write(0, 9, 'Replication Group Id', style)
    sheet5.write(0, 10, 'Cache Subnet Group', style)
    sheet5.write(0, 11, 'Security Groups', style)
    sheet5.write(0, 12, 'Parameter Groups', style)
    sheet5.write(0, 13, 'Parameter Groups Sync', style)

    region_list = regions.split('|')


    # go over all regions in list
    for region in region_list:

        """Function for fetching ElastiCache details"""
        # conn = boto3.client(region= region, aws_access_key_id=ACCESS_KEY,
        #                                           aws_secret_access_key=SECRET_KEY)
        conn = boto3.client('elasticache',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region
                            )
        if not conn:
            sys.stderr.write('Could not connect to region: %s. Skipping\n' % region)
            continue

        paginator = conn.get_paginator('describe_cache_clusters')

        page_iterator = paginator.paginate()

        for page in page_iterator:

        #data = conn.describe_cache_clusters()
            clusters = page["CacheClusters"]

            for value in clusters:
                print value["CacheClusterId"]
                print("filling sheet...")
                sheet5.write(i, j, i)
                j = j + 1
                sheet5.write(i, j, region)
                j = j + 1
                sheet5.write(i, j, value['CacheClusterId'])
                j = j + 1
                sheet5.write(i, j, value['CacheClusterStatus'])
                j = j + 1
                sheet5.write(i, j, value['CacheNodeType'])
                j = j + 1
                sheet5.write(i, j, value['Engine'])
                j = j + 1
                sheet5.write(i, j, value['EngineVersion'])
                j = j + 1
                sheet5.write(i, j, value['PreferredAvailabilityZone'])
                j = j + 1
                sheet5.write(i, j, value['NumCacheNodes'])
                j = j + 1
                if 'ReplicationGroupId' in value:
                    sheet5.write(i, j, value['ReplicationGroupId'])
                    j = j + 1
                else:
                    sheet5.write(i, j, "NA")
                    j = j + 1
                sheet5.write(i, j, value['CacheSubnetGroupName'])
                j = j + 1

                for x in value['SecurityGroups']:
                    sheet5.write(i, j, x['SecurityGroupId'] + x['Status'])
                # sheet5.write(i, j,)
                    j = j + 1

                for y in value['CacheParameterGroup'].keys():
                # print "Values", y
                    if y == 'CacheParameterGroupName':
                        #     cache = value[y]
                        # print cache['CacheParameterGroupName']
                        print "Values of Key", value['CacheParameterGroup'][y]
                        sheet5.write(i, j, value['CacheParameterGroup'][y])
                        j = j + 1
                    if y == 'ParameterApplyStatus':
                        print "Values of Key", value['CacheParameterGroup'][y]
                        sheet5.write(i, j, value['CacheParameterGroup'][y])

                i = i + 1
                j = 0
                print i
                print j
                print("here")

    wb.save(filepath)
    f.close()
    return True


def get_regions ():

    client = boto3.client('ec2')
    #regions = [region['RegionName']
    region_det = client.describe_regions()

    chk_regions = []
    i = 0
    for reg_info in region_det['Regions']:

        chk_regions.append(str(reg_info['RegionName']))
        i = i + 1
    return chk_regions




#def rds_details ():
    #
    #
    # # opens file
    # f = open_file(filepath,"RDS Details")
    # if not f:
    #     return False
    #
    # regions = "ap-southeast-1"
    #
    # sheet4 = wb.add_sheet('RDS')
    # style = xlwt.easyxf('font: bold 1,color red;')
    # sheet4.write(0, 0, 'Sr.No', style)
    # sheet4.write(0, 1, 'Region', style)
    # sheet4.write(0, 2, 'AZ', style)
    # sheet4.write(0, 3, 'Database Name', style)
    # sheet4.write(0, 4, 'Allocated Storage', style)
    # sheet4.write(0, 5, 'Engine', style)
    # sheet4.write(0, 6, 'Engine Version', style)
    # sheet4.write(0, 7, 'Multi-AZ', style)
    # sheet4.write(0, 8, 'Backup and Retention Period', style)
    # sheet4.write(0, 9, 'License Model', style)
    # sheet4.write(0, 10, 'Instance Class', style)
    # sheet4.write(0, 11, 'Create Time', style)
    # sheet4.write(0, 12, 'Publicly Accessible', style)
    # sheet4.write(0, 13, 'Endpoint', style)
    # sheet4.write(0, 14, 'Master Username', style)
    #
    # volume_dictRDS = {}
    # region_list = regions.split('|')
    #
    # i = 1
    # j = 0
    #
    #
    # # go over all regions in list
    # for region in region_list:
    #
    #
    #         #rds = boto.rds.connect_to_region(region, aws_access_key_id=ACCESS_KEY, aws_secret_access_key=SECRET_KEY)
    #         # client = boto3.client('rds')
    #
    #     client = boto3.client('rds',
    #                           aws_access_key_id=ACCESS_KEY,
    #                           aws_secret_access_key=SECRET_KEY,
    #                           region_name=region
    #                           )
    #     volume_dictRDS[region] = {}
    #     #instances = client.describe_db_instances()
    #
    #     # Create a reusable Paginator
    #     paginator = client.get_paginator('describe_db_instances')
    #
    #     page_iterator = paginator.paginate()
    #
    #     for page in page_iterator:
    #
    #         for dbi in page['DBInstances']:
    #             #pprint(dbi)
    #             #pprint(dbi.DBName)
    #
    #             try:
    #                 DBName = dbi['DBName'],
    #                 print(DBName)
    #             except:
    #                 DBName = u''
    #             try:
    #                 publicly_accessible = dbi['PubliclyAccessible'],
    #             except:
    #                 publicly_accessible = u''
    #             try:
    #                 allocated_storage = dbi['AllocatedStorage'],
    #             except:
    #                 allocated_storage = 0
    #             try:
    #                 availability_zone = dbi['AvailabilityZone'],
    #             except:
    #                 availability_zone = u''
    #             try:
    #                 backup_retention_period = dbi['BackupRetentionPeriod'],
    #             except:
    #                 backup_retention_period = u''
    #             try:
    #                 multi_az = dbi['MultiAZ'],
    #             except:
    #                 multi_az = u''
    #             try:
    #                 master_username = dbi['MasterUsername'],
    #             except:
    #                 master_username = u''
    #             try:
    #                 license_model = dbi['LicenseModel'],
    #             except:
    #                 license_model = u''
    #             try:
    #                 instance_class = dbi['DBInstanceClass'],
    #             except:
    #                 instance_class = u''
    #             try:
    #                 engine = dbi['Engine'],
    #             except:
    #                 engine = u''
    #             try:
    #                 engine_version = dbi['EngineVersion'],
    #             except:
    #                 engine_version = u''
    #             try:
    #                 endpoint = dbi['Endpoint']['Address'],
    #             except:
    #                 endpoint = u''
    #             try:
    #                 create_time = dbi['InstanceCreateTime'],
    #             except:
    #                 create_time = u''
    #
    #             volume_dictRDS[region][dbi['DBInstanceIdentifier']] = {'DBName': DBName,
    #                                         'publiclyAccessible': publicly_accessible,
    #                                         'allocated_storage': allocated_storage,
    #                                         'availability_zone': availability_zone,
    #                                         'backup_retention_period': backup_retention_period,
    #                                         'multi_az': multi_az,
    #                                         'master_username': master_username,
    #                                         'license_model': license_model,
    #                                         'instance_class': instance_class,
    #                                         'engine': engine,
    #                                         'engine_version': engine_version,
    #                                         'endpoint': endpoint,
    #                                         'create_time': create_time
    #                                         }
    #
    #
    #     for rds_det in volume_dictRDS[region].keys():
    #
    #             rds2 = volume_dictRDS[region][rds_det]
    #             print("filling sheet...")
    #             print i
    #             print j
    #             sheet4.write(i, j, i)
    #             j = j + 1
    #             sheet4.write(i, j, region)
    #             j = j + 1
    #             sheet4.write(i, j, rds2['availability_zone'])
    #             j = j + 1
    #             sheet4.write(i, j, str(rds2['DBName']))
    #             j = j + 1
    #             sheet4.write(i, j, str(rds2['allocated_storage']))
    #             j = j + 1
    #             sheet4.write(i, j, rds2['engine'])
    #             j = j + 1
    #             sheet4.write(i, j, rds2['engine_version'])
    #             j = j + 1
    #             sheet4.write(i, j, str(rds2['multi_az']))
    #             j = j + 1
    #             sheet4.write(i, j, str(rds2['backup_retention_period']))
    #             j = j + 1
    #             sheet4.write(i, j, rds2['license_model'])
    #             j = j + 1
    #             sheet4.write(i, j, rds2['instance_class'])
    #             j = j + 1
    #             sheet4.write(i, j, str(rds2['create_time']))
    #             j = j + 1
    #             sheet4.write(i, j, str(rds2['publiclyAccessible']))
    #             j = j + 1
    #             sheet4.write(i, j, rds2['endpoint'])
    #             j = j + 1
    #             sheet4.write(i, j, rds2['master_username'])
    #
    #             i = i + 1
    #             j = 0
    #             print i
    #             print j
    #             print("here")
    #
    # wb.save(filepath)
    # f.close()
    #
    # return True


def rds_details ():


    # opens file
    f = open_file(filepath,"RDS Details")
    if not f:
        return False

    sheet4 = wb.add_sheet('RDS')
    style = xlwt.easyxf('font: bold 1,color red;')
    sheet4.write(0, 0, 'Sr.No', style)
    sheet4.write(0, 1, 'Region', style)
    sheet4.write(0, 2, 'AZ', style)
    sheet4.write(0, 3, 'Database Name', style)
    sheet4.write(0, 4, 'Allocated Storage', style)
    sheet4.write(0, 5, 'Engine', style)
    sheet4.write(0, 6, 'Engine Version', style)
    sheet4.write(0, 7, 'Multi-AZ', style)
    sheet4.write(0, 8, 'Backup and Retention Period', style)
    sheet4.write(0, 9, 'License Model', style)
    sheet4.write(0, 10, 'Instance Class', style)
    sheet4.write(0, 11, 'Create Time', style)
    sheet4.write(0, 12, 'Publicly Accessible', style)
    sheet4.write(0, 13, 'Endpoint', style)
    sheet4.write(0, 14, 'Master Username', style)

    volume_dictRDS = {}
    region_list = regions.split('|')

    i = 1
    j = 0


    # go over all regions in list
    for region in region_list:

        client = boto3.client('rds',
                              aws_access_key_id=ACCESS_KEY,
                              aws_secret_access_key=SECRET_KEY,
                              region_name=region
                              )
        #volume_dictRDS[region] = {}
        #instances = client.describe_db_instances()

        # Create a reusable Paginator
        paginator = client.get_paginator('describe_db_instances')

        page_iterator = paginator.paginate()

        for page in page_iterator:

            for dbi in page['DBInstances']:
                #pprint(dbi)
                #pprint(dbi.DBName)

                try:
                    DBName = dbi['DBName'],
                    print(DBName)
                except:
                    DBName = u''
                try:
                    publicly_accessible = dbi['PubliclyAccessible'],
                except:
                    publicly_accessible = u''
                try:
                    allocated_storage = dbi['AllocatedStorage'],
                except:
                    allocated_storage = 0
                try:
                    availability_zone = dbi['AvailabilityZone'],
                except:
                    availability_zone = u''
                try:
                    backup_retention_period = dbi['BackupRetentionPeriod'],
                except:
                    backup_retention_period = u''
                try:
                    multi_az = dbi['MultiAZ'],
                except:
                    multi_az = u''
                try:
                    master_username = dbi['MasterUsername'],
                except:
                    master_username = u''
                try:
                    license_model = dbi['LicenseModel'],
                except:
                    license_model = u''
                try:
                    instance_class = dbi['DBInstanceClass'],
                except:
                    instance_class = u''
                try:
                    engine = dbi['Engine'],
                except:
                    engine = u''
                try:
                    engine_version = dbi['EngineVersion'],
                except:
                    engine_version = u''
                try:
                    endpoint = dbi['Endpoint']['Address'],
                except:
                    endpoint = u''
                try:
                    create_time = dbi['InstanceCreateTime'],
                except:
                    create_time = u''


                print("filling sheet...")
                print i
                print j
                sheet4.write(i, j, i)
                j = j + 1
                sheet4.write(i, j, region)
                j = j + 1
                sheet4.write(i, j, availability_zone)
                j = j + 1
                sheet4.write(i, j, DBName)
                j = j + 1
                sheet4.write(i, j, str(allocated_storage))
                j = j + 1
                sheet4.write(i, j, engine)
                j = j + 1
                sheet4.write(i, j, engine_version)
                j = j + 1
                sheet4.write(i, j, str(multi_az))
                j = j + 1
                sheet4.write(i, j, str(backup_retention_period))
                j = j + 1
                sheet4.write(i, j, license_model)
                j = j + 1
                sheet4.write(i, j, instance_class)
                j = j + 1
                sheet4.write(i, j, str(create_time))
                j = j + 1
                sheet4.write(i, j, str(publicly_accessible))
                j = j + 1
                sheet4.write(i, j, endpoint)
                j = j + 1
                sheet4.write(i, j, master_username)

                i = i + 1
                j = 0
                print i
                print j
                print("here")

    wb.save(filepath)
    f.close()

    return True


def one():
    return ec2_details ()

def two():
    return ebs_details ()

def three():
    return s3_details ()

def four():
    return rds_details ()

def five():
    return elb_details()

def six():
    return iam_details ()

def seven():
    return redis_details()

def eight():
    return cloudfront_details()

def nine():
    return dynamodb_details()

def ten():
    return nat_details()

def eleven():
    return security_group_details()

def thirteen():
    return lambda_details()

def fourteen():
    return cloudtrail_details()

def fifteen():
    return route53_details()

def sixteen():
    return sns_details()

def seventeen():
    return sqs_details()

def eightteen():
    return acm_details()

def nighteen():
    return redshift_details()

def twenty():
    return elb_classic_details()

def twentyone():
    return api_gateway_details()

def  switch(argument):

        switcher = {
            1: one,
            2: two,
            3: three,
            4: four,
            5: five,
            6: six,
            7: seven,
            8: eight,
            9: nine,
            10: ten,
            11: eleven,
            13: thirteen,
            14: fourteen,
            15: fifteen,
            16: sixteen,
            17: seventeen,
            18: eightteen,
            19: nighteen,
            20: twenty,
            21: twentyone
        }
        # Get the function from switcher dictionary
        func = switcher.get(argument, lambda: "Invalid case")
        # Execute the function
        return func()

if __name__ == '__main__':
    # ec2_connection (ACCESS_KEY,SECRET_KEY,regions)

    parser = argparse.ArgumentParser(description='Creates a Excel report about AWS Resources.')
    parser.add_argument('--access_key',required = True, help='AWS API access key.  If missing default is used')
    parser.add_argument('--secret_key',required = True, help='AWS API secret key.  If missing default is used')
    parser.add_argument('--file', required=True, help='Path for output Excel file')
    parser.add_argument('--regions', required=True, help='AWS regions to create the report on, can add multiple with | as separator. Default will assume all regions')
    args = parser.parse_args()

    #args.access_key,args.secret_key,args.file

    ACCESS_KEY = args.access_key
    SECRET_KEY = args.secret_key
    filepath = args.file
    regions = args.regions
    if(regions.lower() == 'all'):
        regions = 'ap-southeast-1|us-east-2|us-east-1|us-west-1|us-west-2|ap-south-1|ap-northeast-2|ap-southeast-2|ap-northeast-1|ca-central-1|eu-central-1|eu-west-1|eu-west-2'
    else:
        regions = args.regions

    print(regions)
    #Uncomment below to use EC2 Details
    switch(1)
    #Uncomment below to use EBS Details
    switch(2)
    #Uncomment below to use S3 Details
    switch(3)
    #Uncomment below to use RDS Details
    switch(4)
    #Uncomment below to use ALB Details
    switch(5)
    #Uncomment below to use IAM Details
    switch(6)
    #Uncommet below to use REDIS Details
    switch(7)
    #Uncomment below to use CLOUDFRONT Details
    switch(8)
    #Uncomment below to use DYNAMODB Details
    switch(9)
    #Uncomment below to use NAT Gateway Details
    switch(10)
    #Uncomment below to use Security Group Details
    switch(11)
    # Uncomment below to get Lambda Details
    switch(13)
    #Uncomment below to get CloudTrail Details
    switch(14)
    # Uncomment below to get Route53 Details
    switch(15)
    # Uncomment below to get SNS Details
    switch(16)
    # Uncomment below to get SQS Details
    switch(17)
    # Uncomment below to get ACM Details
    switch(18)
    # Uncomment below to get Redshift Details
    switch(19)
    # Uncomment below to use Classic ALB Details
    switch(20)
    # Uncomment below to use API Gateway Details
    switch(21)
